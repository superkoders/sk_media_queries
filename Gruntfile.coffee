module.exports = (grunt) ->
	# require('time-grunt')(grunt)
	require('jit-grunt')(grunt,
		spritepacker: 'grunt-sprite-packer'
		# imagesize: 'sk-imgsize'
	)

	srcPath = 'src/'
	destPath = 'web/'

	grunt.initConfig(
		pkg: grunt.file.readJSON('package.json')

		browserify:
			options:
				browserifyOptions:
					debug: true
					extensions: ['.coffee']
				watch: false
				# run watchify and contrib-watch together
				keepAlive: false
				transform: [
					"coffeeify"
					"debowerify"
				]
			main:
				src: "#{srcPath}js/app.coffee"
				dest: "#{destPath}js/app.js"

			watch:
				options:
					watch: true
				src: "#{srcPath}js/app.coffee"
				dest: "#{destPath}js/app.js"

		uglify:
			options:
				compress:
					properties: true
					sequences: true
					dead_code: true
					drop_debugger: true
					unsafe: false
					conditionals: true
					evaluate: true
					booleans: true
					loops: true
					unused: true
					if_return: true
					join_vars: true
					cascade: true
					hoist_vars: false
					hoist_funs: true
					drop_console: true

			main:
				src: "#{destPath}js/app.js"
				dest: "#{destPath}js/app.js"

		coffee:
			main:
				expand: true
				flatten: true
				cwd: "#{srcPath}js/static/"
				src: ['**.coffee', '!_no-delete.coffee']
				dest: "#{destPath}js/"
				ext: '.js'

		stylus:
			options:
				compress: true
				linenos: false
				firebug: false
				sourcemap:
					inline: true
			main:
				src: "#{srcPath}css/style.styl"
				dest: "#{destPath}css/style.css"
			print:
				src: "#{srcPath}css/print.styl"
				dest: "#{destPath}css/print.css"

		sass:
			options:
				compass: true
				style: 'compressed'
			main:
				src: "#{srcPath}css/style.scss"
				dest: "#{destPath}css/style.css"
			print:
				src: "#{srcPath}css/print.scss"
				dest: "#{destPath}css/print.css"

		legacssy:
			options:
				legacyWidth: 1000
			legacy:
				src: "#{destPath}css/style.css"
				dest: "#{destPath}css/style-legacy.css"

		cssmin:
			legacy:
				src: "#{destPath}css/style-legacy.css"
				dest: "#{destPath}css/style-legacy.css"

		pixrem:
			options:
				rootvalue: '10px'
				replace: true
			legacy:
				src: "#{destPath}css/style-legacy.css"
				dest: "#{destPath}css/style-legacy.css"



		spritepacker:
			options:
				template: "#{srcPath}css/lib/sprites-tpl/sprites.css.tpl"
				destCss: "#{srcPath}css/lib/sprites/sprites.styl"
				baseUrl: '../img/bg/'
				padding: 2
				evenPixels: true
			main:
				src: [
					"#{srcPath}img/bg/sprites/*.png"
					"!#{srcPath}img/bg/sprites/_no-delete.png"
				]
				dest: "#{destPath}img/bg/sprite.png"

			retina:
				options:
					template: "#{srcPath}css/lib/sprites-tpl/sprites-retina.css.tpl"
					destCss: "#{srcPath}css/lib/sprites/sprites-retina.styl"
				src: [
					"#{srcPath}img/bg/sprites-retina/*.png"
					"!#{srcPath}img/bg/sprites-retina/_no-delete.png"
				]
				dest: "#{destPath}img/bg/sprites-retina.png"

		copy:
			export:
				expand: true
				cwd: destPath
				src: '**'
				dest: '_export/'
			images:
				expand: true
				cwd: "#{srcPath}img/"
				src: [
					"**/*"
					"!**/sprites*"
					"!**/sprites*/*"
				]
				dest: "#{destPath}img/"
			js:
				expand: true
				cwd: "#{srcPath}js/static/"
				src: [
					'**/*.js'
					'!_no-delete.js'
				]
				dest: "#{destPath}js/"
			root:
				expand: true
				cwd: "#{srcPath}"
				src: [
					'**/*'
					'!css/**/*'
					'!js/**/*'
					'!part/**/*'
					'!img/**/*'
					'!tpl/**/*'
				]
				dest: "#{destPath}"
				filter: 'isFile'


		clean:
			main:
				src: destPath

		compress:
			main:
				options:
					archive: '_zip/'
				expand: true
				cwd: '_export/'
				src: ''
				dest: ''

		php2html:
			options:
				htmlhint:
					'tagname-lowercase': false
					'attr-lowercase': false
					'attr-value-double-quotes': false
					'doctype-first': false
					'tag-pair': false
					'spec-char-escape': false
					'id-unique': false
					'src-not-empty': false
			main:
				expand: true
				cwd: "#{srcPath}tpl/"
				src: '*.php'
				dest: "#{destPath}tpl/"
				ext: '.html'

		'sk-imgsize':
			options:
				quote: '"'
			main:
				expand: true
				cwd: "#{destPath}/tpl/"
				src: '*.html'
				dest: "#{destPath}/tpl/"
				ext: '.html'

		imagemin:
			main:
				expand: true
				cwd: "img/"
				src: '**/*.{png,jpg,gif}'
				dest: 'img/'

		watch:
			options:
				debounceDelay: 49
				livereload: false
			stylmain:
				files: [
					"#{srcPath}css/**/*.styl"
					"!#{srcPath}css/print.styl"
					"!#{srcPath}css/print/**/*.styl"
				]
				tasks: ['stylus:main']
			stylprint:
				files: [
					"#{srcPath}css/print.styl"
					"#{srcPath}css/print/**/*.styl"
					"#{srcPath}css/lib.styl"
					"#{srcPath}css/lib/**/*.styl"
				]
				tasks: ['stylus:print']
			php:
				files: ["#{srcPath}**/*.php"]
				tasks: ['php2html', 'sk-imgsize']
			spritesmain:
				files: ["#{srcPath}img/bg/sprites/*.png"]
				tasks: ['spritepacker:main']
			spritesretina:
				files: ["#{srcPath}img/bg/sprites-retina/*.png"]
				tasks: ['spritepacker:retina']
			images:
				files: [
					"#{srcPath}img/**/*"
					"!#{srcPath}img/**/sprites*"
					"!#{srcPath}img/**/sprites*/*"
				]
				tasks: ['copy:images']
			staticjs:
				files: ["#{srcPath}js/static/**.js"]
				tasks: ['copy:js']
			staticcoffee:
				files: ["#{srcPath}js/static/**.coffee"]
				tasks: ['coffee:main']
			root:
				files: [
					"#{srcPath}**/*"
					"!#{srcPath}*.php"
					"!#{srcPath}css/**/*"
					"!#{srcPath}js/**/*"
					"!#{srcPath}part/**/*"
					"!#{srcPath}img/**/*"
				]
				tasks: ['copy:root']
			csslegacy:
				files: ["#{destPath}css/style.css"]
				tasks: ['legacssy:legacy', 'cssmin:legacy', 'pixrem:legacy']
			dest:
				options:
					livereload: true
				files: ["#{destPath}**/*"]

			gruntfile:
				options:
					reload: true
				files: [
					'Gruntfile.coffee'
					'package.json'
				]
	)

	# Default task
	grunt.registerTask('default', [
		'build'
		'browserify:watch'
		'watch'
	])

	grunt.registerTask('build', [
		'clean:main'
		'copy:js'
		'copy:images'
		'copy:root'
		'coffee:main'
		'spritepacker'
		'stylus'
		'legacssy:legacy'
		'cssmin:legacy'
		'pixrem:legacy'
		'php2html'
		'sk-imgsize'
		'browserify:main'
	])

	# Minifikace projektu
	grunt.registerTask('min', 'project minification', ->
		grunt.config('stylus.options.sourcemap', null)

		grunt.task.run([
			'build'
			'uglify'
		])
	)


	# Export projektu
	grunt.registerTask('export', 'project export', ->

		# jméno složky
		folder = grunt.config('pkg.name') + '-' + grunt.template.today("yyyy-mm-dd-HH.MM")
		path_export = grunt.config('copy.export.dest') + folder + '/'

		# nastavení složky pro export
		grunt.config('copy.export.dest',  path_export)

		# nastavení minifikace obrázků
		grunt.config('imagemin.main.cwd', path_export + grunt.config('imagemin.main.cwd') )
		grunt.config('imagemin.main.dest', path_export + grunt.config('imagemin.main.dest') )

		# nastavení zdroje pro zip
		grunt.config('compress.main.src', folder + '/**')
		# nastavení jména zipu
		grunt.config('compress.main.options.archive', grunt.config('compress.main.options.archive') + folder + '.zip')

		# grunt.log.writeflags(obj, prefix)
		grunt.task.run([
			'min'
			'copy:export'
			'imagemin'
			'compress'
		])
	)
