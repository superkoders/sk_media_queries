<?php
	$title = 'Obecná podstránka';
	$pageClass = 'page-subpage';
	include 'part/header.php';
?>

		<div id="main">
			<div class="row-main">
				<h1>Obecné styly - typografie</h1>
				<h2>Nadpis h2</h2>
				<p><img src="../img/illust/sample.jpg" alt="" class="r" /> Suspendisse <a href="#">odkaz v textu</a> vel nisi accumsan pretium. Etiam id massa ut neque iaculis auctor. Fermentum eu dictum risus consequat. Cras magna justo, iaculis non scelerisque eget, condimentum in tellus. Maecenas a sem quam. Suspendisse facilisis neque tempor enim imperdiet sodales. Vitae lobortis sapien vestibulum. Suspendisse vehicula libero vel nisi accumsan. Suspendisse vehicula libero vel nisi accumsan pretium. Etiam id massa ut neque iaculis auctor. Fermentum eu dictum risus consequat. Cras magna justo, iaculis non scelerisque eget, condimentum in tellus. Maecenas a sem quam. Suspendisse.</p>
				<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assuiber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
				<h3>Nadpis h3</h3>
				<ul>
					<li><strong>Etiam ante sem,</strong> porta a porttitor ut, varius varius metus.</li>
					<li><strong>Nunc eu felis</strong> quis metus volutpat pellentesque.</li>
					<li><strong>Duis gravida</strong> tincidunt enim sed cursus.</li>
					<li><strong>Nunc eu felis</strong> quis metus volutpat pellentesque.</li>
					<li><strong>Duis gravida</strong> tincidunt enim sed cursus.</li>
				</ul>
				<ol>
					<li><strong>Etiam ante sem,</strong> porta a porttitor ut, varius varius metus.</li>
					<li><strong>Nunc eu felis</strong> quis metus volutpat pellentesque.</li>
					<li><strong>Duis gravida</strong> tincidunt enim sed cursus.</li>
					<li><strong>Nunc eu felis</strong> quis metus volutpat pellentesque.</li>
					<li><strong>Duis gravida</strong> tincidunt enim sed cursus.</li>
				</ol>
				<p>Suspendisse <a href="#">odkaz v textu</a> vel nisi accumsan pretium. Etiam id massa ut neque iaculis auctor. Fermentum eu dictum risus consequat. Cras magna justo, iaculis non scelerisque eget, condimentum in tellus. Maecenas a sem quam. Suspendisse facilisis neque tempor enim imperdiet sodales. Vitae lobortis sapien vestibulum. Suspendisse vehicula libero vel nisi accumsan. Suspendisse vehicula libero vel nisi accumsan pretium. Etiam id massa ut neque iaculis auctor. Fermentum eu dictum risus consequat. Cras magna justo, iaculis non scelerisque eget, condimentum in tellus. Maecenas a sem quam. Suspendisse.</p>
				<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assuiber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
				<blockquote>
					<p>Bloková citace - Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta</p>
				</blockquote>
				<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.<br />Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam <strong>liber tempor cum soluta nobis</strong> eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
				<h4>Nadpis 4 úrovně</h4>
				<ol>
					<li>Číslovaný seznam</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
					<li>tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, uis nostrud exercitation ullamco laboris nisi ut iquip x ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse</li>
					<li>cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
				</ol>
				<hr />
				<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
				<dl>
					<dt>Definiční seznam</dt>
					<dd>Hodnota definice</dd>
					<dt>Definiční seznam</dt>
					<dd>Hodnota definice</dd>
					<dt>Definiční seznam</dt>
					<dd>Hodnota definice</dd>
				</dl>
				<h5>Nadpis 5 úrovně</h5>
				<a href="#" class="ext">Ext. odkaz</a>
				<h6>Nadpis 6 úrovně</h6>
				<p><cite>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</cite></p>
				<table>
					<caption>Nadpis tabulky</caption>
					<thead>
						<tr>
							<th id="th1BD4E9800000">Rozlišení</th>
							<th id="th1BD4E9800001">Úhlopříčka</th>
							<th id="th1BD4E9800002">Záruka</th>
							<th id="th1BD4E9800003">Výška</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td headers="th1BD4E9800000">ff</td>
							<td headers="th1BD4E9800001">fff</td>
							<td headers="th1BD4E9800002">fdsd</td>
							<td headers="th1BD4E9800003">fsdf</td>
						</tr>
						<tr>
							<td headers="th1BD4E9800000">ff</td>
							<td headers="th1BD4E9800001">fff</td>
							<td headers="th1BD4E9800002">fdsd</td>
							<td headers="th1BD4E9800003">fsdf</td>
						</tr>
						<tr>
							<td headers="th1BD4E9800000">ff</td>
							<td headers="th1BD4E9800001">fff</td>
							<td headers="th1BD4E9800002">fdsd</td>
							<td headers="th1BD4E9800003">fsdf</td>
						</tr>
						<tr>
							<td headers="th1BD4E9800000">ff</td>
							<td headers="th1BD4E9800001">fff</td>
							<td headers="th1BD4E9800002">fdsd</td>
							<td headers="th1BD4E9800003">fsdf</td>
						</tr>
						<tr>
							<td headers="th1BD4E9800000">ff</td>
							<td headers="th1BD4E9800001">fff</td>
							<td headers="th1BD4E9800002">fdsd</td>
							<td headers="th1BD4E9800003">fsdf</td>
						</tr>
						<tr>
							<td headers="th1BD4E9800000">ff</td>
							<td headers="th1BD4E9800001">fff</td>
							<td headers="th1BD4E9800002">fdsd</td>
							<td headers="th1BD4E9800003">fsdf</td>
						</tr>
						<tr>
							<td headers="th1BD4E9800000">ff</td>
							<td headers="th1BD4E9800001">fff</td>
							<td headers="th1BD4E9800002">fdsd</td>
							<td headers="th1BD4E9800003">fsdf</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

<?php
	include 'part/footer.php';
?>
