<?php
	error_reporting('E_ALL ^ E_NOTICE');
?>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html lang="cs" class="ie7 no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="cs" class="ie8 no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="cs" class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="cs" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="author" content="HTML by SuperKodéři (info@superkoderi.cz)" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" /> -->
		<title><?php echo $title ?> | Project</title>
		<!--[if gt IE 8]><!-->
		<link rel="stylesheet" href="../css/style.css" media="screen, projection" />
		<!--<![endif]-->
	    <!--[if lte IE 8]>
	    <link rel="stylesheet" href="../css/style-legacy.css" media="screen, projection" />
	    <![endif]-->
		<link rel="stylesheet" href="../css/print.css" media="print" />
		<link rel="shortcut icon" href="../favicon.ico?v=1" />
		<script>document.documentElement.className = document.documentElement.className.replace('no-js', 'js');</script>
	</head>
	<body class="<?php echo $pageClass ?>">

		<p id="menu-accessibility">
			<a title="Přejít k obsahu (Klávesová zkratka: Alt + 2)" accesskey="2" href="#main">Přejít k obsahu</a>
			<span class="hide">|</span>
			<a href="#menu-main">Přejít k hlavnímu menu</a>
			<span class="hide">|</span>
			<a href="#form-search">Přejít k vyhledávání</a>
		</p>

		<div id="header">
			<div class="row-main">
				<?php if($pageClass == 'page-homepage'){ ?>
				<h1 id="logo">
					<img src="../img/logo-superkoderi.png" alt="SuperKodéři" />
				</h1>
				<?php }else{ ?>
				<p id="logo">
					<a href="#">
						<img src="../img/logo-superkoderi.png" alt="SuperKodéři" />
					</a>
				</p>
				<?php } ?>
			</div>
		</div>

		<div id="menu-main">
			<div class="row-main">
				<ul class="reset">
					<li><a href="#">Úvod</a></li>
					<li><a href="#">Kábrman roku</a></li>
					<li><a href="#">Poradna</a></li>
					<li><a href="#">Pro kutily</a></li>
					<li><a href="#">Kontakt</a></li>
				</ul>
			</div>
		</div>
