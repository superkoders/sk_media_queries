$ = require('jquery')
window.jQuery ?= $

mq = require('./MediaQueries')

window.App =
	run: ->
		test = new mq(
			'queries':
				'0':
					name: 'mobile'
				'760':
					name: 'tablet'
		)

		console.log(test.init())

		return

