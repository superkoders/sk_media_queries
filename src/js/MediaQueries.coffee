$ = require('jquery')
Events = require('sk-events')
keys = require('lodash/object/keys')
debounce = require('lodash/function/debounce')

class MediaQueries extends Events
	constructor: (options) ->
		super

		@options = $.extend(
			'timer': 250
			'queries': {}
			'retina': true
		, options)
		@isRetina = null
		@isInited = false

	init: ->
		@W = window
		@$W = $( @W )

		if not @W.matchMedia
			msg = 'browser not support matchMedia, please use polyfill
			https://github.com/paulirish/matchMedia.js/ or for full support
			responsive https://github.com/scottjehl/Respond'

			console.log(msg)
			return

		@addQueries( @options.queries )
		@state = null
		@params = null
		@matches = null
		@isRetina = @options.retina and @checkRetina()

		@width = @$W.width()
		@height = @$W.height()

		@$W.on('resize.MediaQueries', debounce(@check, @options.timer))

		@check()

		@isInited = true
		@trigger('init' )

		return @

	destroy: ->
		@$W.off('.MediaQueries')

		return @

	matchMedia: (media) ->
		return @W.matchMedia("#{media}").matches

	getMatches: ->
		ret = []
		for query in @queriesArr
			if @matchMedia("(min-width:#{query}px)")
				ret.push( query )

		return ret

	getQuery: (matches) ->
		return if matches.length then matches[ matches.length - 1 ] else null

	addQueries: (queries) ->
		@state = null
		@queries = $.extend(true, {}, @queries or {}, queries)
		@queriesArr = $.map( keys( @queries ), (element) ->
			element * 1
		)
		.sort((a, b) -> a - b)

		return @

	check: =>
		matches = @getMatches()
		query = @getQuery(matches)
		oldState = @state

		if query? and oldState isnt query
			@state = query
			@params = @queries[ query ]
			@matches = matches

			@trigger('change',
				'state': @state
				'fromState': oldState
				'params': @params
				'matches': @matches
				'retina': @isRetina
			)

		width = @$W.width()
		height = @$W.height()

		@trigger('resize',
			state: query
			isWidth: width isnt @width
			isHeight: height isnt @height
		)

		@width = width
		@height = height

	checkRetina: ->
		mediaQuery = '
			(-webkit-min-device-pixel-ratio: 1.5),
			(min--moz-device-pixel-ratio: 1.5),
			(-o-min-device-pixel-ratio: 3/2),
			(min-resolution: 1.5dppx)
		'

		return window.devicePixelRatio > 1 or @matchMedia(mediaQuery) or false

module.exports = MediaQueries
